package ru.edu.lecture1920;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lecture1920Application {

	public static void main(String[] args) {
		SpringApplication.run(Lecture1920Application.class, args);
	}

}
